const combineSelectors = require('postcss-combine-duplicated-selectors');
const purgecss = require('@fullhuman/postcss-purgecss')({
  content: ['./assets/index.html', './src/**/*.tsx'],
  defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || []
});

module.exports = {
  plugins: [
    require('postcss-flexbugs-fixes'),
    require('autoprefixer')({ flexbox: 'no-2009' }),
    ...(process.env.NODE_ENV === 'production'
      ? [purgecss, combineSelectors({ removeDuplicatedProperties: true })]
      : [])
  ]
};
