import React from 'react';

import { Header } from './header';
import Logo from './logo.svg';

export const Root = () => {
  return (
    <>
      <Header />
      <main>
        <Logo />
        <h1>What happens next is up to you...</h1>
      </main>
    </>
  );
};
