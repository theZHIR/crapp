import React from 'react';

export const Header = () => {
  const [count, setCount] = React.useState(0);

  React.useEffect(() => {
    const counter = setInterval(() => setCount(count + 1), 1000);
    return () => clearInterval(counter);
  });

  return (
    <header>
      <p>HMR Check</p>
      <p>Count: {count}</p>
    </header>
  );
};
