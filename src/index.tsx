import React from 'react';
import { render } from 'react-dom';
import { hot } from 'react-hot-loader';

import { Root } from './root';

const rootElement = document.getElementById('root') as HTMLElement;

const App = hot(module)(Root);

const renderApp = () => {
  render(<App />, rootElement);
};

renderApp();

if (module && module.hot) {
  module.hot.accept(renderApp);
}
